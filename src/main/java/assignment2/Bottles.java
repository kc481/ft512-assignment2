package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		String plural1 = " bottles";
		String plural2 = " bottles";
		String string1,string2,string3,string4;
		if (verseNumber == 1) {
			plural1 = " bottle";
		}
		if (verseNumber == 2) {
			plural2 = " bottle";
		}
		string1 = verseNumber + plural1 + " of beer on the wall, ";
		string2 = verseNumber + plural1 + " of beer.\n";
		string3 = "Take one down and pass it around, ";
		string4 = (verseNumber - 1) + plural2 + " of beer on the wall.\n";
		if (verseNumber == 0) {
			string1 = "No more bottles of beer on the wall, ";
			string2 = "no more bottles of beer.\n";
			string3 = "Go to the store and buy some more, ";
			string4 = "99 bottles of beer on the wall.\n";
		}
		if (verseNumber == 1) {
			string3 = "Take it down and pass it around, ";
			string4 = "no more bottles of beer on the wall.\n";
		}
		return string1 + string2 + string3 + string4;
	}
	public String verse(int startVerseNumber, int endVerseNumber) {
		String largestring = "";
		for(int x = startVerseNumber; x > endVerseNumber; x--) {
			largestring = largestring + this.verse(x) + "\n";
		}
		largestring = largestring + this.verse(endVerseNumber);
		return largestring;
	}

	public String song() {
		String songstring = this.verse(99,0);
		return songstring;
	}
}
